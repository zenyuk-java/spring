package com.example.reactor;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/")
public class Controller {
  @GetMapping("/{name}")
  public Mono<String> getMovieByTitle(@PathVariable String name) {

    WebClient client = WebClient.create("http://www.google.com/jkjk");
    var request = client.get();
    Mono<String> s = Mono.empty();
    s = request
        .retrieve()
        .bodyToMono(String.class)
        .onErrorResume(t -> secondTry(t));
    System.out.println("leaving try");

    return s;
  }


  public Mono<String> secondTry(Throwable t) {
    System.out.println("inside secondTry");
    WebClient client = WebClient.create("http://www.google.com");
    var request = client.get();
    return request
        .retrieve()
        .bodyToMono(String.class);
  }
}
