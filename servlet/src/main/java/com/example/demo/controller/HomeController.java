package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
public class HomeController {

  @GetMapping("/tools")
  @ResponseBody
  public List<String> findAll() {
    var s = Stream.of(2, 4, 3, 5, 1, 3);
    "".matches()
    var result = s.sorted()
        .filter(e -> e > 1)
        .distinct()
        .map(e -> Integer.toString(e * 2))
        .collect(Collectors.toList());
    return result;
  }
}
