package com.example.demo;

import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("properties")
@NoArgsConstructor
@Data
public class Properties {
  private String bookStore;
  private int fee;
}
