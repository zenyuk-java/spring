package com.example.demo.controller;

import com.example.demo.model.Book;
import com.example.demo.service.BookService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("book")
public class BookController {

  private final BookService bookService;

  public BookController(BookService bookService) {
    this.bookService = bookService;
  }

  @GetMapping("/{vendor}-{id}")
  public Book getBookById(@PathVariable int id, @PathVariable String vendor) {
    return bookService.orderBook(id, vendor);
  }
}
