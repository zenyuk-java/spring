package com.example.demo.service;

import com.example.demo.model.Book;

public interface BookService {
    Book orderBook(int id, String vendor);
}
