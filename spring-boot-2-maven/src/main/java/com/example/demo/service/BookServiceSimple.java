package com.example.demo.service;

import com.example.demo.Properties;
import com.example.demo.exception.UnknownVendorException;
import com.example.demo.model.Book;
import com.example.demo.vendor_processor.RequestToProcess;
import com.example.demo.vendor_processor.VendorProcessor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class BookServiceSimple implements BookService {
  private static final String DEFAULT_SHOP = "central";
  private final Properties properties;
  private List<VendorProcessor> vendorProcessors;
  // alternatively use only one explicitly
  private VendorProcessor explicitProcessor;

  public BookServiceSimple(
      Properties properties,
      List<VendorProcessor> vendorProcessors,
      @Qualifier("ScratchPressProcessor") VendorProcessor explicitProcessor) {
    this.properties = properties;
    this.vendorProcessors = vendorProcessors;
    this.explicitProcessor = explicitProcessor;
  }

  @Override
  public Book orderBook(int id, String vendor) {
    String name = "some-book-" + id + "-" + DEFAULT_SHOP;

    // process all vendors
    RequestToProcess req =
        new RequestToProcess(Integer.toString(id), LocalDateTime.now(), properties.getBookStore());

    System.out.println("Book Service: processing vendor: " + vendor);
    vendorProcessors.stream()
            .filter(v -> v.getVendorName().equals(vendor))
            .findFirst()
            .orElseThrow(() -> new UnknownVendorException("Unknown vendor: " + vendor))
            .process(req);

    // alternatively - explicit via qualifier
    // explicitProcessor.process(req);

    return Book.builder().name(name).vendor(vendor).build();
  }
}
