package com.example.demo.service;

import java.util.List;
import java.util.Map;

public interface StoreService {
    List<String> getAllStoreNames();
    Map<String, Integer> getBookPricePerStore(String bookId);
    int getCheapestPrice(String bookId);
}
