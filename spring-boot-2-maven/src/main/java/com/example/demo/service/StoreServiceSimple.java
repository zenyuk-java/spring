package com.example.demo.service;

import java.util.*;

public class StoreServiceSimple implements StoreService {
  @Override
  public List<String> getAllStoreNames() {
    return null;
  }

  @Override
  public Map<String, Integer> getBookPricePerStore(String bookId) {
    return Map.of("Shop1", -12, "Shop2", -1, "Shop3", 0, "Shop4", 1, "Shop5", 2);
  }

    @Override
    public int getCheapestPrice(String bookId) {
        Map<String, Integer> allPrices = getBookPricePerStore(bookId);
        OptionalInt result = allPrices.values().stream().mapToInt(v -> v).max();
        result.orElseThrow(() -> new IllegalArgumentException("Not found bookId: " + bookId));
        return result.getAsInt();

        // both of the below will not work
        // return allPrices.values().stream().max(Math::max).get();
        // return allPrices.values().stream().max(Integer::max).get();
    }


}
