package com.example.demo.service;

public class WeirdJavaImpl implements WeirdJava {
    @Override
    public Double twoPlusThree() {
        double x = 2.8;
        Double y = 0.17;
        return x + y;
    }
}
