package com.example.demo.vendor_processor;

public enum ProcessedStatus {
    OK,
    ERROR,
    UNKNOWN
}
