package com.example.demo.vendor_processor;

import lombok.Value;

import java.time.LocalDateTime;

@Value
public class RequestToProcess {
    private String bookId;
    private LocalDateTime startDateTime;
    private String store;
}
