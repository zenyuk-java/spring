package com.example.demo.vendor_processor;

public interface VendorProcessor {
    ProcessedStatus process(RequestToProcess request);
    String getVendorName();
}
