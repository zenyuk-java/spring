package com.example.demo.vendor_processor.vendor_impl;

import com.example.demo.vendor_processor.ProcessedStatus;
import com.example.demo.vendor_processor.RequestToProcess;
import com.example.demo.vendor_processor.VendorProcessor;
import org.springframework.stereotype.Component;

@Component("OreillyProcessor")
public class VendorProcessorOreilly implements VendorProcessor {
  @Override
  public ProcessedStatus process(RequestToProcess request) {
    System.out.println("Oreilly has been processed");
    return ProcessedStatus.OK;
  }

  @Override
  public String getVendorName() {
    return "Oreilly";
  }
}
