package com.example.demo.vendor_processor.vendor_impl;

import com.example.demo.vendor_processor.ProcessedStatus;
import com.example.demo.vendor_processor.RequestToProcess;
import com.example.demo.vendor_processor.VendorProcessor;
import org.springframework.stereotype.Component;

@Component("ScratchPressProcessor")
public class VendorProcessorScratchPress implements VendorProcessor {
  @Override
  public ProcessedStatus process(RequestToProcess request) {
    System.out.println("Scratch Press has been processed");
    return ProcessedStatus.OK;
  }

  @Override
  public String getVendorName() {
    return "ScratchPress";
  }
}
