package com.example.demo.service;

import com.example.demo.Properties;
import com.example.demo.model.Book;
import com.example.demo.vendor_processor.vendor_impl.VendorProcessorOreilly;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * {@link BookServiceSimple}
 * */
class BookServiceSimpleTest {
//TODO:@Mock
  private static Properties properties;

//TODO:  @Rule public MockitoRule mockitoRule = MockitoJUnit.rule();

  @BeforeEach
  void setUp() {
    properties = new Properties();
  }

  @Test
  void orderBook_returnsBook_givenValidInput() {
    // arrange
    BookService classUnderTest = new BookServiceSimple(properties, new ArrayList<>(), new VendorProcessorOreilly());
    String vendor = "ScratchPress";
    int validBookId = 5;

    // act
    Book result = classUnderTest.orderBook(validBookId, vendor);

    // assert
    assertNotNull(result);
    assertTrue(StringUtils.hasLength(result.getName()));
  }
}