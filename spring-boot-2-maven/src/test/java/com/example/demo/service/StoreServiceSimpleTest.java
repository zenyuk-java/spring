package com.example.demo.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
* {@link StoreServiceSimple}
*/
class StoreServiceSimpleTest {

  @Test
  void getCheapestPrice() {
    // arrange
    StoreService classUnderTest = new StoreServiceSimple();

    // act
    Integer result = classUnderTest.getCheapestPrice("somebook");

    // assert
    assertEquals(2, result);
  }
}