package com.example.demo.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WeirdJavaImplTest {

  @Test
  void twoPlusThree() {
    // arrange
    var classUnderTest = new WeirdJavaImpl();
    final double WEIRD_SUM = 2.9699999999999998;

    // act
    var result = classUnderTest.twoPlusThree();


    // assert
    assertEquals(WEIRD_SUM, result);
  }
}